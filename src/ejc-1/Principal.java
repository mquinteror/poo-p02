/* clase principal que contiene al metodo main, recibe y comprueba que los
   los parametros sean pares */
// se facilita el manejo de los argumentos usando un arraylist
class Principal {
    String[] argumentos = null;
    public Principal ( String[] argumentos ){
	this.argumentos = argumentos;
    }
    
    public void verificarParidaddeParametros (){
	// primero verificar que no sean 0 los argumentos
	System.out.println("argumento 0" + argumentos[0] + argumentos.length);
	if ( this.argumentos.length != 0 ){
	    // hay argumentos, probar la paridad
	    if ( this.argumentos.length%2 == 0 ){
		// los argumentos son pares :)
		System.out.println("son pares");
	    }
	    else {
		// son impares, hay que quitar 1 :(
		System.out.println("son impares");
		
	    }
	}
    }

    public static void main ( String args[] ){
	System.out.println("hola la clase esta viva");
	Principal princ = new Principal( args );
	princ.verificarParidaddeParametros ();
    }
}
